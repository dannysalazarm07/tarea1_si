import fileinput

entrada = []
unariasA = []
unariasB = []
binariasA = []
binariasB = []
binariasC = []
binariasX = []
n = 0
m = 0
li = ''
deadline = 0


def lectura():
    for line in fileinput.input("entrada"):
        a = line.rstrip('\n')
        if (a.rstrip('/n')):
            entrada.append(''.join(a.split()))

    global n
    n = int(entrada[0])
    global m
    m = int(entrada[1])
    global li
    li = entrada[2]
    global deadline
    deadline = entrada[3]
    print "Entrada =", entrada
    print

def separaRestriccionesUnarias():
    for i in range(len(entrada)):
        if(entrada[i] == 'Restriccionesbinarias'):
            valor = i

    unarias = entrada[5:valor]
    binarias = entrada[valor+1:]

    for i in unarias:
        for j in range(len(i)):
            if(i[j] == '='):
                tipo1 = j
                unariasA.append(i)
            elif(i[j] == '!'):
                unariasB.append(i)

    for i in unarias:
        if(i[1]== '='):
            unariasA.append(i)

    print "Unarias =", unarias
    print "Binarias =", binarias

    print

    print "UnariasA =", unariasA
    print "UnariasB =", unariasB

    print

def separaRestriccionesBinarias():

    contarComas = 0

    for i in range(len(entrada)):
        if(entrada[i] == 'Restriccionesbinarias'):
            valor = i

    unarias = entrada[5:valor]
    binarias = entrada[valor+1:]

    for i in binarias:
        for j in range(len(i)):
            if(i[j] == ','):
                contarComas += 1
        if(contarComas == 2):
            binariasC.append(i)
        else:
            binariasX.append(i)
        contarComas = 0

    for i in binariasX:
        for j in range(len(i)):
            if(i[j] == '='):
                binariasA.append(i)
            elif(i[j] == '!'):
                binariasB.append(i)

    for i in unarias:
        if(i[1]== '='):
            unariasA.append(i)

    print "bA =", binariasA
    print "bB =", binariasB
    print "bC =", binariasC

if __name__ == '__main__':
    print
    lectura()

    print "n = ", n
    print "m = ", m
    print "li = ", li
    print "deadline = ", deadline
    print

    separaRestriccionesUnarias()
    separaRestriccionesBinarias()
